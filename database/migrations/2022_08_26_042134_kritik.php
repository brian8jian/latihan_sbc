<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kritik', function (Blueprint $table) {
            $table->id();
            $table->text('content');
            $table->integer('point');
            // $table->foreignId('user_id');
            // $table->foreignId('film_id');
            //     $table->unsignedBigInteger('user_id');
            //     $table->foreign('user_id')->refrences('id')->on('user');
            //     $table->unsignedBigInteger('film_id');
            //     $table->foreign('film_id')->refrences('id')->on('film');
            $table->foreignId('user_id')->constrained('user')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('film_id')->constrained('film')->onUpdate('cascade')->onDelete('cascade');
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kritik');
        //
    }
};
