<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peran', function (Blueprint $table) {
            $table->id();
            $table->string('nama', 45);
            // $table->foreignId('film_id');
            // $table->foreignId('cast_id');
            // $table->unsignedBigInteger('film_id');
            // $table->foreign('film_id')->refrences('id')->on('film');
            // $table->unsignedBigInteger('cast_id');
            // $table->foreign('cast_id')->refrences('id')->on('cast');
            $table->foreignId('film_id')->constrained('film')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('cast_id')->constrained('cast')->onUpdate('cascade')->onDelete('cascade');
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peran');
        //
    }
};
