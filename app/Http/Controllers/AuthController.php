<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }    //
    public function welcome(Request $request)
    {
        // dd($request->all()); //menampilkan isi post
        $ftname = $request['ftname'];
        $ltname = $request['ltname'];
        return view('welcome', [
            'ftname' => $ftname,
            'ltname' => $ltname,
        ]);
    }
}
