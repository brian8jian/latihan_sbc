<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function index()
    {
        $casts = DB::table('cast')->get();
        return view('contents.cast.cast', [
            'casts' => $casts
        ]);
    }
    public function create()
    {
        return view('contents.cast.cast-tambah');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }
    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        // return view('contents.cast.cast-show', compact('cast'));
        return view('contents.cast.cast-show', [
            'cast' => $cast
        ]);
    }
    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('contents.cast.cast-edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }
    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
