@extends('layouts.main')
@section('container')
    <form action="/welcome" method="post">
        @csrf
        <h3>Sign Up Form</h3>
        <label for="ftname">First name :</label><br /><br />
        <input type="text" name="ftname" id="ftname" /><br /><br />
        <label for="ltname">Last name :</label><br /><br />
        <input type="text" name="ltname" id="ltname" /><br /><br />
        <label for="">Gender</label><br /><br />
        <input type="radio" name="gender" id="gdmale" />Male <br />
        <input type="radio" name="gender" id="gdfemale" />Female <br />
        <input type="radio" name="gender" id="gdoth" />Other <br />
        <p>Nationality :</p>
        <br />
        <select>
            <option value="">Indonesian</option>
            <option value="">Jepang</option>
            <option value="">Malaysia</option>
            <option value="">Singapura</option>
            <option value="">Inggris</option>
        </select>
        <br /><br />
        <label for="">Languange Spoken:</label><br /><br />
        <input type="checkbox" id="lgindo" value="1" name="spoke" />
        <label for="lgindo">Bahasa Indonesia</label><br />
        <input type="checkbox" id="lginggris" value="2" name="spoke" />
        <label for="lginggris">Bahasa Inggris</label><br />
        <input type="checkbox" id="lgoth" value="3" name="spoke" />
        <label for="lgoth">Bahasa Other</label><br />
        <label for="bio">Bio:</label>
        <br />
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br />
        <button type="submit">Sign Up</button>
    </form>
@endsection
