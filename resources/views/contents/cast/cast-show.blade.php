@extends('layouts.master')
@section('content-header')
    <h1>Halaman Caster</h1>
@endsection
@section('content-main-judul')
    <h3 class="card-title">Halaman Caster</h3>
@endsection
@section('content-main')
    <div class="card">
        <div class="card-body text-center">
            <h2>Show Post {{ $cast->id }}</h2>
            <h4>{{ $cast->nama }}</h4>
            <p>{{ $cast->umur }}</p>
            <p>{{ $cast->bio }}</p>
            <a href="/cast" class="my-3 btn btn-primary">kembali</a>
        </div>
    </div>
@endsection
