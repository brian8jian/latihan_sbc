@extends('layouts.master')
@section('content-header')
    <h1>Halaman Edit Caster</h1>
@endsection
@section('content-main-judul')
    <h3 class="card-title">Halaman Edit Caster</h3>
@endsection
@section('content-main')
    <div class="card">
        <div class="card-body ">
            {{-- <h2>Edit Post {{ $cast->id }}</h2> --}}
            <form action="/cast/{{ $cast->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{ $cast->nama }}" id="nama"
                        placeholder="Masukkan nama">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" name="umur" value="{{ $cast->umur }}" id="umur"
                        placeholder="Masukkan umur">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" name="bio" value="{{ $cast->bio }}" id="bio"
                        placeholder="Masukkan bio">
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="row justify-content-between">
                    <button type="submit" class="btn btn-primary">Edit</button>
                    <a href="/cast" class="btn btn-info">Kembali</a>
                    {{-- <div class="col">
                    </div>
                    <div class="col">
                    </div> --}}
                </div>
            </form>
        </div>
    </div>
@endsection
