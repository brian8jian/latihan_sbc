@extends('layouts.master')
@section('content-header')
    <h1>Halaman Caster</h1>
@endsection
{{-- @push('script')
    <script src="{{ asset('template/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.js"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
        });
    </script>
@endpush
@push('my-style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css" />
@endpush --}}
@section('content-main-judul')
    <h3 class="card-title">Halaman Caster</h3>
@endsection
@section('content-main')
    <div class="card">
        <div class="card-body">
            <a href="/cast/create" class="my-3 btn btn-primary">Tambah</a>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Umur</th>
                        <th scope="col">Bio</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($casts as $key=>$value)
                        <tr>
                            <td>{{ $key + 1 }}</th>
                            <td>{{ $value->nama }}</td>
                            <td>{{ $value->umur }}</td>
                            <td>{{ $value->bio }}</td>
                            <td>
                                <a href="/cast/{{ $value->id }}" class="btn btn-info">Show</a>
                                <a href="/cast/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                                <form action="/cast/{{ $value->id }}" method="POST" style="display: inline-block">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
{{-- DONE --}}
